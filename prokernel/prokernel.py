import socket
import json


class EntityValue:
    position_x: float
    position_y: float
    angle: float

    def __init__(self, json_object):
        self.position_x = json_object["x"]
        self.position_y = json_object["y"]
        self.angle = json_object["angle"]

class Position:
    x: float
    y: float

    def __init__(self, json_object):
        self.x = json_object["x"]
        self.y = json_object["y"]

class Control:

    def __init__(self, throttle: float, wing: float):
        """
        @param throttle: float representing throttle command between 0 and 1
        @param wing: float representing wing command between -1 and 1
        """
        self.throttle = throttle
        self.wing = wing

    def get_dict(self):
        return {
            "throttle": self.throttle,
            "wing": self.wing
        }


class WorldState:
    user_missile: EntityValue
    enemy_missile: EntityValue
    target: Position

    def __init__(self, json_dict):
        """
        @param json_dict: json composed with fields of this object
        """
        self.user_missile = EntityValue(json_dict["userMissile"])
        self.enemy_missile = EntityValue(json_dict["enemyMissile"])
        self.target = Position(json_dict["targetPosition"])


def init(addr: str, port: int):
    global client

    print(f"Connection to Battletor at {addr}:{port}")
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect((addr, port))
    print(f"Connection established")
    

def receive():
    global client

    new_message = client.recv(1024)
    new_message_json = json.loads(new_message)
    print("Json arrived : ", new_message_json)

    world_state = WorldState(new_message_json)
    return world_state


def send(control: Control):
    print("Sending a new control : ", json.dumps(control.get_dict()))
    client.send(json.dumps(control.get_dict()).encode("utf-8"))
