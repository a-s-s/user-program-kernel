import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name='prokernel',
    version='0.0.9',
    author='Ass team',
    description='Testing installation of Package',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/a-s-s/user-program-kernel.git',
    license='MIT',
    packages=['prokernel'],
    install_requires=['requests', 'Vector2'],
)
